# Reviewing and Modifying a Table Definition<a name="EN-US_TOPIC_0289900185"></a>

-   **[Overview](overview-13.md)**  

-   **[Selecting a Storage Model](selecting-a-storage-model-14.md)**  

-   **[Using PCKs](using-pcks.md)**  

-   **[Using Partitioned Tables](using-partitioned-tables-15.md)**  

-   **[Selecting a Data Type](selecting-a-data-type-16.md)**  


