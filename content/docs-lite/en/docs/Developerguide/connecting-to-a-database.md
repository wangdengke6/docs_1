# Connecting to a Database<a name="EN-US_TOPIC_0289900104"></a>

Client tools used for connecting to a database include  **gsql**  and APIs \(such as  **JDBC**\).

-   **gsql**  is a client tool provided by openGauss. As described in  [Using gsql to Connect to a Database](using-gsql-to-connect-to-a-database.md),  **psql**  is used to enter, edit, and run SQL statements in an interactive manner.
-   As described in  [APIs](apis.md), standard databases, such as  **JDBC**, can be used to develop openGauss-based applications.

-   **[Confirming Connection Information](confirming-connection-information.md)**  

-   **[Configuring Remote Connection](configuring-remote-connection.md)**  

-   **[Using gsql to Connect to a Database](using-gsql-to-connect-to-a-database.md)**  

-   **[APIs](apis.md)**  


