# STATEMENT\_USER\_COMPLEX\_HISTORY<a name="EN-US_TOPIC_0289900004"></a>

**STATEMENT\_USER\_COMPLEX\_HISTORY**  displays load management information about completed jobs executed on the current primary database node. Data is dumped from the kernel to this system catalog. Columns in this view are the same as those in  [Table 1](gs_session_memory_detail.md#en-us_topic_0059778760_td16c4d9490d3429bb7924dc70121414a).

