# libpq<a name="EN-US_TOPIC_0289899861"></a>

-   **[Database Connection Control Functions](database-connection-control-functions.md)**  

-   **[Database Statement Execution Functions](database-statement-execution-functions.md)**  

-   **[Functions for Asynchronous Command Processing](functions-for-asynchronous-command-processing.md)**  

-   **[Functions for Canceling Queries in Progress](functions-for-canceling-queries-in-progress.md)**  


