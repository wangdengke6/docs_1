# APIs<a name="EN-US_TOPIC_0289900152"></a>

You can use standard database APIs, such as  **JDBC**, to develop openGauss-based applications.

## Supported APIs<a name="en-us_topic_0283136775_en-us_topic_0237120293_en-us_topic_0059777757_s3ddc9e88cb7f4367bb98c43d4b658e05"></a>

Each application is an independent openGauss development project. APIs alleviate applications from directly operating in databases, and enhance the database portability, extensibility, and maintainability.  [Table 1](#en-us_topic_0283136775_en-us_topic_0237120293_en-us_topic_0059777757_tc44f4815cb564ea182d5864daa2709b4)  lists the APIs supported by openGauss and the download addresses.

**Table  1**  Database APIs

<a name="en-us_topic_0283136775_en-us_topic_0237120293_en-us_topic_0059777757_tc44f4815cb564ea182d5864daa2709b4"></a>
<table><thead align="left"><tr id="en-us_topic_0283136775_en-us_topic_0237120293_en-us_topic_0059777757_r073d74c6283a460d92711f61bfb41a99"><th class="cellrowborder" valign="top" width="16.919999999999998%" id="mcps1.2.3.1.1"><p id="en-us_topic_0283136775_en-us_topic_0237120293_en-us_topic_0059777757_aff1f63cf5f17429e8de6ea5187f60ba7"><a name="en-us_topic_0283136775_en-us_topic_0237120293_en-us_topic_0059777757_aff1f63cf5f17429e8de6ea5187f60ba7"></a><a name="en-us_topic_0283136775_en-us_topic_0237120293_en-us_topic_0059777757_aff1f63cf5f17429e8de6ea5187f60ba7"></a>API</p>
</th>
<th class="cellrowborder" valign="top" width="83.08%" id="mcps1.2.3.1.2"><p id="en-us_topic_0283136775_en-us_topic_0237120293_en-us_topic_0059777757_a81074fed55284b89a2f8402dbacc62ba"><a name="en-us_topic_0283136775_en-us_topic_0237120293_en-us_topic_0059777757_a81074fed55284b89a2f8402dbacc62ba"></a><a name="en-us_topic_0283136775_en-us_topic_0237120293_en-us_topic_0059777757_a81074fed55284b89a2f8402dbacc62ba"></a>How to Obtain</p>
</th>
</tr>
</thead>
<tbody><tr id="en-us_topic_0283136775_en-us_topic_0237120293_en-us_topic_0059777757_r424aac31ed6449fea384dd79a6ad0cf1"><td class="cellrowborder" valign="top" width="16.919999999999998%" headers="mcps1.2.3.1.1 "><p id="en-us_topic_0283136775_en-us_topic_0237120293_en-us_topic_0059777757_aa4ddc323690a4d1ca9b06eed4da2f91e"><a name="en-us_topic_0283136775_en-us_topic_0237120293_en-us_topic_0059777757_aa4ddc323690a4d1ca9b06eed4da2f91e"></a><a name="en-us_topic_0283136775_en-us_topic_0237120293_en-us_topic_0059777757_aa4ddc323690a4d1ca9b06eed4da2f91e"></a>JDBC</p>
</td>
<td class="cellrowborder" valign="top" width="83.08%" headers="mcps1.2.3.1.2 "><a name="en-us_topic_0283136775_en-us_topic_0237120293_en-us_topic_0059777757_ua42cb60c35a2468c80ac3965a7e26649"></a><a name="en-us_topic_0283136775_en-us_topic_0237120293_en-us_topic_0059777757_ua42cb60c35a2468c80ac3965a7e26649"></a><ul id="en-us_topic_0283136775_en-us_topic_0237120293_en-us_topic_0059777757_ua42cb60c35a2468c80ac3965a7e26649"><li>Driver: <span id="en-us_topic_0283136775_en-us_topic_0237120293_text18477673291"><a name="en-us_topic_0283136775_en-us_topic_0237120293_text18477673291"></a><a name="en-us_topic_0283136775_en-us_topic_0237120293_text18477673291"></a>openGauss-x.x</span>-EULER-64bit-Jdbc.tar.gz</li><li>Driver: org.postgresql.Driver</li></ul>
</td>
</tr>
</tbody>
</table>

You can use  **JDBC**  to connect to the database. Therefore, you need to  [configure a remote connection](configuring-remote-connection.md)  in openGauss.

For details about more APIs, see  [Application Development Guide](application-development-guide.md).

