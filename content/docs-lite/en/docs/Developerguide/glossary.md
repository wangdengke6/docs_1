# Glossary<a name="EN-US_TOPIC_0289899826"></a>

<a name="en-us_topic_0283137351_en-us_topic_0276131909_table38686068"></a>
<table><thead align="left"><tr id="en-us_topic_0283137351_en-us_topic_0276131909_row35886203"><th class="cellrowborder" valign="top" width="23.23%" id="mcps1.1.3.1.1"><p id="en-us_topic_0283137351_en-us_topic_0276131909_p21101319"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p21101319"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p21101319"></a>Acronym</p>
</th>
<th class="cellrowborder" valign="top" width="76.77000000000001%" id="mcps1.1.3.1.2"><p id="en-us_topic_0283137351_en-us_topic_0276131909_p31485239"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p31485239"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p31485239"></a>Definition/Description</p>
</th>
</tr>
</thead>
<tbody><tr id="en-us_topic_0283137351_en-us_topic_0276131909_row167539"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p13570724"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p13570724"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p13570724"></a>2PL</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p25486898"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p25486898"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p25486898"></a>2-Phase Locking</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row28055495"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p57902635"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p57902635"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p57902635"></a>ACID</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p59601822"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p59601822"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p59601822"></a>Atomicity, Consistency, Isolation, Durability</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row66654358"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p30293926"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p30293926"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p30293926"></a>AP</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p37888922"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p37888922"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p37888922"></a>Analytical Processing</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row5455981"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p39281331"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p39281331"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p39281331"></a>ARM</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p27671249"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p27671249"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p27671249"></a>Advanced RISC Machine, a hardware architecture alternative to x86</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row47714657"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p39682040"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p39682040"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p39682040"></a>CC</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p60128634"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p60128634"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p60128634"></a>Concurrency Control</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row4286800"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p11686496"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p11686496"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p11686496"></a>CPU</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p7082129"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p7082129"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p7082129"></a>Central Processing Unit</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row63739165"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p62598754"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p62598754"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p62598754"></a>DB</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p37334299"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p37334299"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p37334299"></a>Database</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row464375"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p37614394"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p37614394"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p37614394"></a>DBA</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p26867045"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p26867045"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p26867045"></a>Database Administrator</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row40476817"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p57396781"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p57396781"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p57396781"></a>DBMS</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p18627652"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p18627652"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p18627652"></a>Database Management System</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row33431146"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p23568328"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p23568328"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p23568328"></a>DDL</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p29986390"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p29986390"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p29986390"></a>Data Definition Language. Database Schema management language</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row1442054"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p49697568"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p49697568"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p49697568"></a>DML</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p66080062"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p66080062"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p66080062"></a>Data Modification Language</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row57849650"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p55310098"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p55310098"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p55310098"></a>ETL</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p50932971"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p50932971"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p50932971"></a>Extract, Transform, Load or Encounter Time Locking</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row55743556"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p18934148"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p18934148"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p18934148"></a>FDW</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p57270992"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p57270992"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p57270992"></a>Foreign Data Wrapper</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row45676887"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p8840353"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p8840353"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p8840353"></a>GC</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p44980012"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p44980012"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p44980012"></a>Garbage Collector</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row2166929"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p41303584"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p41303584"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p41303584"></a>HA</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p57256003"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p57256003"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p57256003"></a>High Availability</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row45541984"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p65022077"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p65022077"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p65022077"></a>HTAP</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p32296866"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p32296866"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p32296866"></a>Hybrid Transactional-Analytical Processing</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row22236344"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p56313405"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p56313405"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p56313405"></a>IoT</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p65091934"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p65091934"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p65091934"></a>Internet of Things</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row48956501"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p6053677"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p6053677"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p6053677"></a>IM</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p20585813"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p20585813"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p20585813"></a>In-Memory</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row51054590"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p41781154"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p41781154"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p41781154"></a>IMDB</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p28830314"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p28830314"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p28830314"></a>In-Memory Database</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row58146239"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p12224953"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p12224953"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p12224953"></a>IR</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p50697164"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p50697164"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p50697164"></a>Intermediate Representation of a source code, used in compilation and optimization</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row53621294"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p48357528"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p48357528"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p48357528"></a>JIT</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p24645678"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p24645678"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p24645678"></a>Just In Time</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row20484518"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p48633258"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p48633258"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p48633258"></a>JSON</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p46979855"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p46979855"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p46979855"></a>JavaScript Object Notation</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row20165517"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p22794214"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p22794214"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p22794214"></a>KV</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p34392051"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p34392051"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p34392051"></a>Key Value</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row41093009"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p40199432"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p40199432"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p40199432"></a>LLVM</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p34928551"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p34928551"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p34928551"></a>Low-Level Virtual Machine, refers to a compilation code or queries to IR</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row45921510"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p28654805"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p28654805"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p28654805"></a>M2M</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p39337857"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p39337857"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p39337857"></a>Machine-to-Machine</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row18496393"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p21812826"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p21812826"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p21812826"></a>ML</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p22008476"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p22008476"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p22008476"></a>Machine Learning</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row63858558"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p5160748"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p5160748"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p5160748"></a>MM</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p15367454"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p15367454"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p15367454"></a>Main Memory</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row4089365"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p62803112"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p62803112"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p62803112"></a>MO</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p53887349"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p53887349"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p53887349"></a>Memory Optimized</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row15224096"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p25192244"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p25192244"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p25192244"></a>MOT</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p27305877"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p27305877"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p27305877"></a>Memory Optimized Tables storage engine (SE), pronounced as /em/ /oh/ /tee/</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row44426301"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p41760635"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p41760635"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p41760635"></a>MVCC</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p27168314"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p27168314"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p27168314"></a>Multi-Version Concurrency Control</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row43188242"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p8586676"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p8586676"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p8586676"></a>NUMA</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p24432171"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p24432171"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p24432171"></a>Non-Uniform Memory Access</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row18562954"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p27204315"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p27204315"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p27204315"></a>OCC</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p56065908"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p56065908"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p56065908"></a>Optimistic Concurrency Control</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row34831132"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p2749429"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p2749429"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p2749429"></a>OLTP</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p21377195"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p21377195"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p21377195"></a>Online Transaction Processing</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row58177028"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p14718799"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p14718799"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p14718799"></a>PG</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p51372075"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p51372075"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p51372075"></a>PostgreSQL</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row59695495"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p3496894"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p3496894"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p3496894"></a>RAW</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p14813018"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p14813018"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p14813018"></a>Reads-After-Writes</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row66208306"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p61272544"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p61272544"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p61272544"></a>RC</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p64128998"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p64128998"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p64128998"></a>Return Code</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row40290074"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p42270552"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p42270552"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p42270552"></a>RTO</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p1362691"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p1362691"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p1362691"></a>Recovery Time Objective</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row12264220"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p53877781"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p53877781"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p53877781"></a>SE</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p2024110"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p2024110"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p2024110"></a>Storage Engine</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row18216990"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p66290058"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p66290058"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p66290058"></a>SQL</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p785643"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p785643"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p785643"></a>Structured Query Language</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row7070795"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p35863542"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p35863542"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p35863542"></a>TCO</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p19265802"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p19265802"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p19265802"></a>Total Cost of Ownership</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row39174490"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p19017117"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p19017117"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p19017117"></a>TP</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p63991517"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p63991517"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p63991517"></a>Transactional Processing</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row39052743"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p9155596"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p9155596"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p9155596"></a>TPC-C</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p3405826"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p3405826"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p3405826"></a>An On-Line Transaction Processing Benchmark</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row30652439"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p66928472"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p66928472"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p66928472"></a>Tpm-C</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p52497183"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p52497183"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p52497183"></a>Transactions-per-minute-C. A performance metric for TPC-C benchmark that counts new-order transactions.</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row2712599"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p18393951"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p18393951"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p18393951"></a>TVM</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p13515039"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p13515039"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p13515039"></a>Tiny Virtual Machine</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row54526492"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p54569732"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p54569732"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p54569732"></a>TSO</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p58072185"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p58072185"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p58072185"></a>Time Sharing Option</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row52887619"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p56038774"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p56038774"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p56038774"></a>UDT</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p42846825"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p42846825"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p42846825"></a>User-Defined Type</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row50077106"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p29713768"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p29713768"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p29713768"></a>WAL</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p58005044"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p58005044"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p58005044"></a>Write Ahead Log</p>
</td>
</tr>
<tr id="en-us_topic_0283137351_en-us_topic_0276131909_row52283348"><td class="cellrowborder" valign="top" width="23.23%" headers="mcps1.1.3.1.1 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p7092825"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p7092825"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p7092825"></a>XLOG</p>
</td>
<td class="cellrowborder" valign="top" width="76.77000000000001%" headers="mcps1.1.3.1.2 "><p id="en-us_topic_0283137351_en-us_topic_0276131909_p37647975"><a name="en-us_topic_0283137351_en-us_topic_0276131909_p37647975"></a><a name="en-us_topic_0283137351_en-us_topic_0276131909_p37647975"></a>A PostgreSQL implementation of transaction logging (WAL - described above)</p>
</td>
</tr>
</tbody>
</table>

