# Uninstallation<a name="EN-US_TOPIC_0289899737"></a>

The openGauss Lite uninstallation process includes clearing the openGauss process and related directories.

>![](public_sys-resources/icon-caution.gif) **CAUTION:** 
>The environment variables generated by running the  **source**  command need to be manually cleared. If the environment variable file is not separated by running the  **source**  command, you need to manually clear the export operation in the  **bashrc**  file. Otherwise, dirty environment variables will be introduced.

## Executing Uninstallation<a name="en-us_topic_0283136478_section1229131371816"></a>

openGauss Lite provides an uninstallation script to help users uninstall the Lite environment.

**Procedure**

1.  Log in as the OS user  **omm**  to the primary node of the database.
2.  Assume that the decompression path is  **\~/openGauss**. Go to the directory generated after the decompression.

    ```
    cd ~/openGauss
    ```

3.  Run the  **uninstall.sh**  command to uninstall the openGauss Lite environment.

    ```
    sh uninstall.sh
    ```

    To clear the corresponding installation and data directories, you need to add the  **--delete-data**  parameter. If the environment variables are separated during the installation and no  **source**  command is executed to make the environment variable file take effect, you need to execute  **source**  so that the script can find the corresponding path.

    ```
    sh uninstall.sh --delete-data 
    ```


**Examples**

Execute the  **uninstall.sh**  script to uninstall the Lite.

```
sh uninstall.sh --delete-data
delete-data is true
cleaning up related processes
clean up related processes success
```

