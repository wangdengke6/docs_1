# Backup and Restoration<a name="EN-US_TOPIC_0289897015"></a>

-   **[Overview](overview.md)**  

-   **[Physical Backup and Restoration](physical-backup-and-restoration.md)**  

-   **[Logical Backup and Restoration](logical-backup-and-restoration.md)**  

-   **[Flashback Restoration](flashback-restoration.md)**  


