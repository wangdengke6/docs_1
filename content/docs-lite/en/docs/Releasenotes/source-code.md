# Source Code<a name="EN-US_TOPIC_0289899190"></a>

openGauss contains the following 12 code repositories. The lite edition does not involve the OM and CM code repositories.

-   Open-source software code repository:  [https://gitee.com/opengauss/openGauss-third\_party](https://gitee.com/opengauss/openGauss-third_party)

-   JDBC driver code repository:  [https://gitee.com/opengauss/openGauss-connector-jdbc](https://gitee.com/opengauss/openGauss-connector-jdbc)

-   ODBC driver code repository:  [https://gitee.com/opengauss/openGauss-connector-odbc](https://gitee.com/opengauss/openGauss-connector-odbc)
-   Database server code repository:  [https://gitee.com/opengauss/openGauss-server](https://gitee.com/opengauss/openGauss-server)
-   Database OM tool code repository:  [https://gitee.com/opengauss/openGauss-OM](https://gitee.com/opengauss/openGauss-OM)
-   CM tool code repository:  [https://gitee.com/opengauss/CM](https://gitee.com/opengauss/CM)
-   DCF code repository:  [https://gitee.com/opengauss/DCF](https://gitee.com/opengauss/DCF)
-   DCC code repository:  [https://gitee.com/opengauss/DCC](https://gitee.com/opengauss/DCC)
-   Plug-in code repository:  [https://gitee.com/opengauss/Plugin](https://gitee.com/opengauss/Plugin)
-   MySQL-to-openGauss migration tool code repository:  [https://gitee.com/opengauss/openGauss-tools-chameleon](https://gitee.com/opengauss/openGauss-tools-chameleon)
-   Prometheus-exporter code repository:  [https://gitee.com/opengauss/openGauss-prometheus-exporter](https://gitee.com/opengauss/openGauss-prometheus-exporter)
-   Document repository:  [https://gitee.com/opengauss/docs](https://gitee.com/opengauss/docs)

