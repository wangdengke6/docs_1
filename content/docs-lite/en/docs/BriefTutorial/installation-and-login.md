# Installation and Login<a name="EN-US_TOPIC_0000001209981908"></a>

openGauss supports simplified installation using scripts. Simplified installation includes standalone installation and installation of one primary and one standby nodes.

-   **[Obtaining and Verifying an Installation Package](obtaining-and-verifying-an-installation-package.md)**  

-   **[准备软硬件安装环境](data01-pa_temp-1-en-us_bookmap_0000001262449423220330105200533-temp-en-us_topic_0000001217616720.md)**  

-   **[安装](data01-pa_temp-1-en-us_bookmap_0000001262449423220330105200533-temp-en-us_topic_0000001262456549.md)**  

-   **[gsql Connection and Usage](gsql-connection-and-usage.md)**  


