# AI4DB：趋势预测<a name="ZH-CN_TOPIC_0000001195431216"></a>

-   **[概述](概述-10.md)**  

-   **[环境部署](环境部署-11.md)**  

-   **[使用指导](使用指导-12.md)**  

-   **[获取帮助](获取帮助-13.md)**  

-   **[命令参考](命令参考-14.md)**  

-   **[常见问题处理](常见问题处理-15.md)**  


