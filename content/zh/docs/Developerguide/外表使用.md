# 外表使用<a name="ZH-CN_TOPIC_0311524276"></a>

openGauss本身支持对表的分区存储，从而加快查询时的过滤。通过HIVE创建表，并以ORC或Parquet文件格式写入HDFS时同样可以指定分区属性。openGauss需要将自身的分区属性同HIVE创建表时的分区属性相结合，保证分区映射及查询的正确性。

openGauss通过CREATE FOREIGN TABLE指定创建表使用的分区信息。CREATE FOREIGN TABLE的详细信息请参见[CREATE FOREIGN TABLE](CREATE-FOREIGN-TABLE.md)。

使用分区方式的注意事项包括：

1.  遵循openGauss建立分区表的原则，PARTITION中的分区列必须为COLUMN包中的定义列，这一点与HIVE建表语法不同。
2.  在openGauss中HDFS外表定义分区数和HIVE建表定义分区语法均对分区列的数目无限制，但是最多只支持对前四层分区进行剪枝。如果表分区列多于四个，openGauss将不会对其余分区列进行剪枝操作。

    openGauss的HDFS外表定义分区列数目应不大于HIVE表中定义的分区列数量。同时定义顺序必须和在HIVE表中分区列定义顺序保持一致。

3.  分区剪枝支持的操作包括大于、小于、不大于、不小于、等于、不等于、IS NULL、IS NOT NULL、ANY、ALL操作。对于二元表达式操作，操作符一边必须是基本列类型，另一边必须是const类型。
4.  外表支持和普通表关联查询。示例如下：

    ```
    openGauss=# CREATE TABLE inner_tbl( a int, b int) WITH(ORIENTATION='column');
    ```

    ```
    NOTICE:  The 'DISTRIBUTE BY' clause is not specified. Using 'a' as the distribution column by default.
    HINT:  Please use 'DISTRIBUTE BY' clause to specify suitable data distribution column.
    CREATE TABLE
    ```

    ```
    openGauss=# DROP FOREIGN TABLE region010;
    ```

    ```
    DROP FOREIGN TABLE
    ```

    ```
    openGauss=# CREATE FOREIGN TABLE region010  (
     R_REGIONKEY  INT4,
     constraint part_11constr_unique unique (R_REGIONKEY) not enforced,
     R_NAME TEXT,
     R_COMMENT TEXT)
     SERVER    hdfs_server
     OPTIONS(format 'orc', foldername '/user/hive/warehouse/gaussdb.db/region_orc11_64stripe', encoding 'GBK')
     DISTRIBUTE BY roundrobin;
    ```

    ```
    NOTICE:  CREATE FOREIGN TABLE / UNIQUE will create constraint "part_11constr_unique" for foreign table "region010"
    CREATE FOREIGN TABLE 
    ```

    ```
    openGauss=# EXPLAIN SELECT * FROM region010, inner_tbl WHERE a=R_REGIONKEY;
    ```

    ```
     id |                   operation                    | E-rows | E-width | E-costs
    ----+------------------------------------------------+--------+---------+---------
      1 | ->  Row Adapter                                |     43 |      76 | 68.71
      2 |    ->  Vector Streaming (type: GATHER)         |     43 |      76 | 68.71
      3 |       ->  Vector Hash Join (4,5)               |     43 |      76 | 66.03
      4 |          ->  Vector Foreign Scan on region010  |    853 |      68 | 52.65
      5 |          ->  Vector Streaming(type: BROADCAST) |     10 |       8 | 10.63
      6 |             ->  CStore Scan on inner_tbl       |     10 |       8 | 10.01
    (6 rows)
    
             Predicate Information (identified by plan id)
    ---------------------------------------------------------------
       3 --Vector Hash Join (4,5)
             Hash Cond: (region010.r_regionkey = inner_tbl.a)
             Generate Bloom Filter On Expr: inner_tbl.a
             Generate Bloom Filter On Index: 0
       4 --Vector Foreign Scan on region010
             Server Type: hdfs
             Filter By Bloom Filter On Expr: region010.r_regionkey
             Filter By Bloom Filter On Index: 0
    (8 rows)
    ```

5.  目前存在一些表达式、函数等不支持下推，由于不能下推导致产生多表连接的查询无法再次转化为plan，最终将导致查询无法执行，需要改写SQL重新执行。例如出现如下错误：This SQL can not be executed because it can not be pushed down to datanode，则需要修改SQL语句重新执行。

