# Checking the Database Status<a name="EN-US_TOPIC_0305491426"></a>

This section describes how to query the database status.

## Procedure<a name="section117172026191017"></a>

1.  Log in to the node as the database user \(for example,  **omm**\) and run the  **source**  command to set the environment variables.
2.  Run the following command to check the database status:

    ```
    gs_om -t status
    ```

3.  Ensure that the database is normal.

