# Before You Start<a name="EN-US_TOPIC_0305491437"></a>

-   **[Upgrade Solution](upgrade-solution.md)**  

-   **[Version Requirements Before the Upgrade \(Upgrade Path\)](version-requirements-before-the-upgrade-(upgrade-path).md)**  

-   **[Upgrade Impact and Constraints](upgrade-impact-and-constraints.md)**  


