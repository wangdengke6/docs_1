# Version Requirements Before the Upgrade \(Upgrade Path\)<a name="EN-US_TOPIC_0305491359"></a>

[Table 1](#table7961729)  lists the version requirements for upgrading openGauss.

**Table  1**  Version Requirements Before the Upgrade \(Upgrade Path\)

<a name="table7961729"></a>
<table><tbody><tr id="row48398424"><td class="cellrowborder" valign="top" width="41.410000000000004%"><p id="p27958252"><a name="p27958252"></a><a name="p27958252"></a>Version</p>
</td>
<td class="cellrowborder" valign="top" width="58.589999999999996%"><p id="p50025933"><a name="p50025933"></a><a name="p50025933"></a>Upgrade Description</p>
</td>
</tr>
<tr id="row5917164"><td class="cellrowborder" valign="top" width="41.410000000000004%"><p id="p138378421424"><a name="p138378421424"></a><a name="p138378421424"></a>Versions earlier than openGauss 1.0.1</p>
</td>
<td class="cellrowborder" valign="top" width="58.589999999999996%"><p id="p33594135"><a name="p33594135"></a><a name="p33594135"></a>It can be upgraded to any version earlier than openGauss 1.0.1.</p>
</td>
</tr>
<tr id="row1699043202811"><td class="cellrowborder" valign="top" width="41.410000000000004%"><p id="p16990230282"><a name="p16990230282"></a><a name="p16990230282"></a>openGauss 1.0.1</p>
</td>
<td class="cellrowborder" valign="top" width="58.589999999999996%"><p id="p89903322814"><a name="p89903322814"></a><a name="p89903322814"></a>It can be upgraded to openGauss 1.1.0.</p>
</td>
</tr>
<tr id="row10729745336"><td class="cellrowborder" valign="top" width="41.410000000000004%"><p id="p473019455316"><a name="p473019455316"></a><a name="p473019455316"></a>openGauss 1.1.0 and later</p>
</td>
<td class="cellrowborder" valign="top" width="58.589999999999996%"><p id="p207301045037"><a name="p207301045037"></a><a name="p207301045037"></a>It can be upgraded to any version later than openGauss 1.1.0.</p>
</td>
</tr>
</tbody>
</table>

>![](public_sys-resources/icon-note.gif) **NOTE:** 
>To view the current version, run the following command:
>```
>gsql -V | --version
>```

