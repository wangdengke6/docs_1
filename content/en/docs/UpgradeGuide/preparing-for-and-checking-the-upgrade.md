# Preparing for and Checking the Upgrade<a name="EN-US_TOPIC_0305491450"></a>

This section describes preparations before the upgrade.

-   **[Upgrade Preparation Checklist](upgrade-preparation-checklist.md)**  

-   **[Collecting Node Information](collecting-node-information.md)**  

-   **[Backing Up Data](backing-up-data.md)**  

-   **[Obtaining the Upgrade Packages](obtaining-the-upgrade-packages.md)**  

-   **[Checking the OS Health Status](checking-the-os-health-status.md)**  

-   **[Checking the Disk Usage of the Database Node](checking-the-disk-usage-of-the-database-node.md)**  

-   **[Checking the Database Status](checking-the-database-status.md)**  


