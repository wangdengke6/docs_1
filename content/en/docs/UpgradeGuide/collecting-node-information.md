# Collecting Node Information<a name="EN-US_TOPIC_0305491433"></a>

Contact the database system administrator to obtain names and IP addresses of database nodes. You have obtained the passwords of users  **root**  and  **omm**  for logging in to the nodes, as shown in  [Table 1](#toc218487220).

**Table  1**  Node information

<a name="toc218487220"></a>
<table><thead align="left"><tr id="row32107897"><th class="cellrowborder" valign="top" width="6.34%" id="mcps1.2.7.1.1"><p id="p50602835"><a name="p50602835"></a><a name="p50602835"></a>No.</p>
</th>
<th class="cellrowborder" valign="top" width="15.18%" id="mcps1.2.7.1.2"><p id="p5188953"><a name="p5188953"></a><a name="p5188953"></a>Node Name</p>
</th>
<th class="cellrowborder" valign="top" width="34.72%" id="mcps1.2.7.1.3"><p id="p17652085"><a name="p17652085"></a><a name="p17652085"></a>Node IP Address</p>
</th>
<th class="cellrowborder" valign="top" width="18.12%" id="mcps1.2.7.1.4"><p id="p52539912"><a name="p52539912"></a><a name="p52539912"></a>Password of User <strong id="b33285994722851"><a name="b33285994722851"></a><a name="b33285994722851"></a>root</strong></p>
</th>
<th class="cellrowborder" valign="top" width="18.12%" id="mcps1.2.7.1.5"><p id="p27874513"><a name="p27874513"></a><a name="p27874513"></a>Password of User <strong id="b120292277022851"><a name="b120292277022851"></a><a name="b120292277022851"></a>omm</strong></p>
</th>
<th class="cellrowborder" valign="top" width="7.5200000000000005%" id="mcps1.2.7.1.6"><p id="p1635924414169"><a name="p1635924414169"></a><a name="p1635924414169"></a>Remarks</p>
</th>
</tr>
</thead>
<tbody><tr id="row49544030"><td class="cellrowborder" valign="top" width="6.34%" headers="mcps1.2.7.1.1 "><p id="p53643460"><a name="p53643460"></a><a name="p53643460"></a>1</p>
</td>
<td class="cellrowborder" valign="top" width="15.18%" headers="mcps1.2.7.1.2 "><p id="p50153003"><a name="p50153003"></a><a name="p50153003"></a>-</p>
</td>
<td class="cellrowborder" valign="top" width="34.72%" headers="mcps1.2.7.1.3 "><p id="p35861434"><a name="p35861434"></a><a name="p35861434"></a>-</p>
</td>
<td class="cellrowborder" valign="top" width="18.12%" headers="mcps1.2.7.1.4 "><p id="p3196531"><a name="p3196531"></a><a name="p3196531"></a>-</p>
</td>
<td class="cellrowborder" valign="top" width="18.12%" headers="mcps1.2.7.1.5 "><p id="p57592428"><a name="p57592428"></a><a name="p57592428"></a>-</p>
</td>
<td class="cellrowborder" valign="top" width="7.5200000000000005%" headers="mcps1.2.7.1.6 "><p id="p3359114421610"><a name="p3359114421610"></a><a name="p3359114421610"></a>-</p>
</td>
</tr>
</tbody>
</table>

