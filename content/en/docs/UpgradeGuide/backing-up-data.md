# Backing Up Data<a name="EN-US_TOPIC_0305491448"></a>

If the upgrade fails, services may be affected. Back up data in advance so that services can be restored as soon as possible after a failure occurs.

For details, see section "Backup and Restoration" in the  _Administrator Guide_.

