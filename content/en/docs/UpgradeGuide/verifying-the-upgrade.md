# Verifying the Upgrade<a name="EN-US_TOPIC_0305491432"></a>

This section describes how to verify the upgrade, including the test cases and detailed operation procedure.

-   **[Verification Checklist](verification-checklist.md)**  

-   **[Querying the Version After Upgrade](querying-the-version-after-upgrade.md)**  

-   **[Checking the Status of the Database for Upgrade](checking-the-status-of-the-database-for-upgrade.md)**  


