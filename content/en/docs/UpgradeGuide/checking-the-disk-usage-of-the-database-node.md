# Checking the Disk Usage of the Database Node<a name="EN-US_TOPIC_0305491447"></a>

You are advised to perform the upgrade when the disk usage of the database node is less than 80%.

