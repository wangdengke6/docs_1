# Important Notes<a name="EN-US_TOPIC_0289899192"></a>

-   For details about technical specifications, see the  _Technical White Paper_.
-   You are advised to deploy one primary node and two standby nodes to ensure reliability and availability.

