# Best Practices<a name="EN-US_TOPIC_0000001166812143"></a>

-   **[Best Practices of Table Design](best-practices-of-table-design.md)**  

-   **[Best Practices of Data Import](best-practices-of-data-import.md)**  

-   **[Best Practices of SQL Queries](best-practices-of-sql-queries.md)**  


