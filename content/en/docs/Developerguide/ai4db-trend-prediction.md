# AI4DB: Trend Prediction<a name="EN-US_TOPIC_0000001195431216"></a>

-   **[Overview](overview-11.md)**  

-   **[Environment Deployment](environment-deployment-12.md)**  

-   **[Usage Guide](usage-guide-13.md)**  

-   **[Obtaining Help Information](obtaining-help-information-14.md)**  

-   **[Command Reference](command-reference-15.md)**  

-   **[Troubleshooting](troubleshooting-16.md)**  


