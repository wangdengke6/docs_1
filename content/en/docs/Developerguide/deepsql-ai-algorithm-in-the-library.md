# DeepSQL: AI Algorithm in the Library<a name="EN-US_TOPIC_0296550271"></a>

The DeepSQL feature is compatible with the MADLib framework and can implement AI algorithms in the database. A complete set of SQL-based machine learning, data mining, and statistics algorithms is provided. Users can directly use SQL statements to perform machine learning. Deep SQL can abstract the end-to-end R&D process from data to models. With the bottom-layer engine and automatic optimization, technical personnel with basic SQL knowledge can complete most machine learning model training and prediction tasks. The entire analysis and processing are running in the database engine. Users can directly analyze and process data in the database without transferring data between the database and other platforms. This avoids unnecessary data movement between multiple environments.

-   **[Overview](overview-25.md)**  

-   **[Environment Deployment](environment-deployment-26.md)**  

-   **[User Guide](user-guide-27.md)**  

-   **[Best Practices](best-practices-28.md)**  

-   **[Troubleshooting](troubleshooting-29.md)**  


