# Command Reference<a name="EN-US_TOPIC_0303986187"></a>

**Table  1**  Command-line parameters

<a name="en-us_topic_0283137279_table628178124515"></a>
<table><thead align="left"><tr id="en-us_topic_0283137279_row162968174512"><th class="cellrowborder" valign="top" width="17.18171817181718%" id="mcps1.2.4.1.1"><p id="en-us_topic_0283137279_p1129138144517"><a name="en-us_topic_0283137279_p1129138144517"></a><a name="en-us_topic_0283137279_p1129138144517"></a>Parameter</p>
</th>
<th class="cellrowborder" valign="top" width="58.33583358335833%" id="mcps1.2.4.1.2"><p id="en-us_topic_0283137279_p2029181454"><a name="en-us_topic_0283137279_p2029181454"></a><a name="en-us_topic_0283137279_p2029181454"></a>Description</p>
</th>
<th class="cellrowborder" valign="top" width="24.48244824482448%" id="mcps1.2.4.1.3"><p id="en-us_topic_0283137279_p6291382451"><a name="en-us_topic_0283137279_p6291382451"></a><a name="en-us_topic_0283137279_p6291382451"></a>Value Range</p>
</th>
</tr>
</thead>
<tbody><tr id="en-us_topic_0283137279_row162915844513"><td class="cellrowborder" valign="top" width="17.18171817181718%" headers="mcps1.2.4.1.1 "><p id="en-us_topic_0283137279_p132968134510"><a name="en-us_topic_0283137279_p132968134510"></a><a name="en-us_topic_0283137279_p132968134510"></a>mode</p>
</td>
<td class="cellrowborder" valign="top" width="58.33583358335833%" headers="mcps1.2.4.1.2 "><p id="en-us_topic_0283137279_p11295814511"><a name="en-us_topic_0283137279_p11295814511"></a><a name="en-us_topic_0283137279_p11295814511"></a>Specified running mode</p>
</td>
<td class="cellrowborder" valign="top" width="24.48244824482448%" headers="mcps1.2.4.1.3 "><p id="en-us_topic_0283137279_p02919804513"><a name="en-us_topic_0283137279_p02919804513"></a><a name="en-us_topic_0283137279_p02919804513"></a><strong id="b9893118194310"><a name="b9893118194310"></a><a name="b9893118194310"></a>start</strong>, <strong id="b1918317118431"><a name="b1918317118431"></a><a name="b1918317118431"></a>stop</strong>, <strong id="b133851313164318"><a name="b133851313164318"></a><a name="b133851313164318"></a>forecast</strong>, <strong id="b1222571612434"><a name="b1222571612434"></a><a name="b1222571612434"></a>show_metrics</strong>, <strong id="b671317186432"><a name="b671317186432"></a><a name="b671317186432"></a>deploy</strong>, and <strong id="b794420205438"><a name="b794420205438"></a><a name="b794420205438"></a>diagnosis</strong></p>
</td>
</tr>
<tr id="row1949293216101"><td class="cellrowborder" valign="top" width="17.18171817181718%" headers="mcps1.2.4.1.1 "><p id="p57047404102"><a name="p57047404102"></a><a name="p57047404102"></a>--user</p>
</td>
<td class="cellrowborder" valign="top" width="58.33583358335833%" headers="mcps1.2.4.1.2 "><p id="p19705240181019"><a name="p19705240181019"></a><a name="p19705240181019"></a>Remote server user</p>
</td>
<td class="cellrowborder" valign="top" width="24.48244824482448%" headers="mcps1.2.4.1.3 "><p id="p192324411812"><a name="p192324411812"></a><a name="p192324411812"></a>N/A</p>
</td>
</tr>
<tr id="en-us_topic_0283137279_row19291888452"><td class="cellrowborder" valign="top" width="17.18171817181718%" headers="mcps1.2.4.1.1 "><p id="en-us_topic_0283137279_p16296874513"><a name="en-us_topic_0283137279_p16296874513"></a><a name="en-us_topic_0283137279_p16296874513"></a>--host</p>
</td>
<td class="cellrowborder" valign="top" width="58.33583358335833%" headers="mcps1.2.4.1.2 "><p id="en-us_topic_0283137279_p13297818451"><a name="en-us_topic_0283137279_p13297818451"></a><a name="en-us_topic_0283137279_p13297818451"></a>IP address of the remote server</p>
</td>
<td class="cellrowborder" valign="top" width="24.48244824482448%" headers="mcps1.2.4.1.3 "><p id="p322194491819"><a name="p322194491819"></a><a name="p322194491819"></a>N/A</p>
</td>
</tr>
<tr id="en-us_topic_0283137279_row18298818455"><td class="cellrowborder" valign="top" width="17.18171817181718%" headers="mcps1.2.4.1.1 "><p id="en-us_topic_0283137279_p82912864518"><a name="en-us_topic_0283137279_p82912864518"></a><a name="en-us_topic_0283137279_p82912864518"></a>--project-path</p>
</td>
<td class="cellrowborder" valign="top" width="58.33583358335833%" headers="mcps1.2.4.1.2 "><p id="p1190222245413"><a name="p1190222245413"></a><a name="p1190222245413"></a>Path of the <strong id="b95433483340"><a name="b95433483340"></a><a name="b95433483340"></a>anomaly_detection</strong> project on the remote server</p>
</td>
<td class="cellrowborder" valign="top" width="24.48244824482448%" headers="mcps1.2.4.1.3 "><p id="p92194419180"><a name="p92194419180"></a><a name="p92194419180"></a>N/A</p>
</td>
</tr>
<tr id="en-us_topic_0283137279_row9294819456"><td class="cellrowborder" valign="top" width="17.18171817181718%" headers="mcps1.2.4.1.1 "><p id="en-us_topic_0283137279_p1829118104514"><a name="en-us_topic_0283137279_p1829118104514"></a><a name="en-us_topic_0283137279_p1829118104514"></a>--role</p>
</td>
<td class="cellrowborder" valign="top" width="58.33583358335833%" headers="mcps1.2.4.1.2 "><p id="en-us_topic_0283137279_p1429208164510"><a name="en-us_topic_0283137279_p1429208164510"></a><a name="en-us_topic_0283137279_p1429208164510"></a>Start role</p>
</td>
<td class="cellrowborder" valign="top" width="24.48244824482448%" headers="mcps1.2.4.1.3 "><p id="p420154491810"><a name="p420154491810"></a><a name="p420154491810"></a><strong id="b1130412204018"><a name="b1130412204018"></a><a name="b1130412204018"></a>agent</strong>, <strong id="b104301114144018"><a name="b104301114144018"></a><a name="b104301114144018"></a>collector</strong>, and <strong id="b817613166405"><a name="b817613166405"></a><a name="b817613166405"></a>monitor</strong></p>
</td>
</tr>
<tr id="en-us_topic_0283137279_row1020015014713"><td class="cellrowborder" valign="top" width="17.18171817181718%" headers="mcps1.2.4.1.1 "><p id="en-us_topic_0283137279_p42004013477"><a name="en-us_topic_0283137279_p42004013477"></a><a name="en-us_topic_0283137279_p42004013477"></a>--metric-name</p>
</td>
<td class="cellrowborder" valign="top" width="58.33583358335833%" headers="mcps1.2.4.1.2 "><p id="p16597796570"><a name="p16597796570"></a><a name="p16597796570"></a>Metric name</p>
</td>
<td class="cellrowborder" valign="top" width="24.48244824482448%" headers="mcps1.2.4.1.3 "><p id="p1419744151813"><a name="p1419744151813"></a><a name="p1419744151813"></a>N/A</p>
</td>
</tr>
<tr id="row179571843104312"><td class="cellrowborder" valign="top" width="17.18171817181718%" headers="mcps1.2.4.1.1 "><p id="p1695734316438"><a name="p1695734316438"></a><a name="p1695734316438"></a>--query</p>
</td>
<td class="cellrowborder" valign="top" width="58.33583358335833%" headers="mcps1.2.4.1.2 "><p id="p195794324318"><a name="p195794324318"></a><a name="p195794324318"></a>Root cause analysis target query</p>
</td>
<td class="cellrowborder" valign="top" width="24.48244824482448%" headers="mcps1.2.4.1.3 "><p id="p195713432434"><a name="p195713432434"></a><a name="p195713432434"></a>N/A</p>
</td>
</tr>
<tr id="row132141840154314"><td class="cellrowborder" valign="top" width="17.18171817181718%" headers="mcps1.2.4.1.1 "><p id="p121420408432"><a name="p121420408432"></a><a name="p121420408432"></a>--start_time</p>
</td>
<td class="cellrowborder" valign="top" width="58.33583358335833%" headers="mcps1.2.4.1.2 "><p id="p19214144054318"><a name="p19214144054318"></a><a name="p19214144054318"></a>Time when the query starts</p>
</td>
<td class="cellrowborder" valign="top" width="24.48244824482448%" headers="mcps1.2.4.1.3 "><p id="p182141240134316"><a name="p182141240134316"></a><a name="p182141240134316"></a>N/A</p>
</td>
</tr>
<tr id="row1941123344314"><td class="cellrowborder" valign="top" width="17.18171817181718%" headers="mcps1.2.4.1.1 "><p id="p14942193384310"><a name="p14942193384310"></a><a name="p14942193384310"></a>--finish_time</p>
</td>
<td class="cellrowborder" valign="top" width="58.33583358335833%" headers="mcps1.2.4.1.2 "><p id="p11970141212459"><a name="p11970141212459"></a><a name="p11970141212459"></a>Time when the query ends</p>
</td>
<td class="cellrowborder" valign="top" width="24.48244824482448%" headers="mcps1.2.4.1.3 "><p id="p194273324316"><a name="p194273324316"></a><a name="p194273324316"></a>N/A</p>
</td>
</tr>
<tr id="en-us_topic_0283137279_row1836561411475"><td class="cellrowborder" valign="top" width="17.18171817181718%" headers="mcps1.2.4.1.1 "><p id="en-us_topic_0283137279_p7365314124713"><a name="en-us_topic_0283137279_p7365314124713"></a><a name="en-us_topic_0283137279_p7365314124713"></a>--forecast-periods</p>
</td>
<td class="cellrowborder" valign="top" width="58.33583358335833%" headers="mcps1.2.4.1.2 "><p id="en-us_topic_0283137279_p1236541444719"><a name="en-us_topic_0283137279_p1236541444719"></a><a name="en-us_topic_0283137279_p1236541444719"></a>Forecast period</p>
</td>
<td class="cellrowborder" valign="top" width="24.48244824482448%" headers="mcps1.2.4.1.3 "><p id="p1240071143417"><a name="p1240071143417"></a><a name="p1240071143417"></a>The value is an integer. The specific value depends on the training data. If this parameter is not provided, the default value <strong id="b10443621153817"><a name="b10443621153817"></a><a name="b10443621153817"></a>100S</strong> is used.</p>
</td>
</tr>
<tr id="row1341411345369"><td class="cellrowborder" valign="top" width="17.18171817181718%" headers="mcps1.2.4.1.1 "><p id="p144151534163613"><a name="p144151534163613"></a><a name="p144151534163613"></a>--freq FREQ</p>
</td>
<td class="cellrowborder" valign="top" width="58.33583358335833%" headers="mcps1.2.4.1.2 "><p id="p184162348369"><a name="p184162348369"></a><a name="p184162348369"></a>Prediction interval</p>
</td>
<td class="cellrowborder" valign="top" width="24.48244824482448%" headers="mcps1.2.4.1.3 "><p id="p1341603493618"><a name="p1341603493618"></a><a name="p1341603493618"></a>The time unit can be <strong id="b1144213315386"><a name="b1144213315386"></a><a name="b1144213315386"></a>S</strong> (second), <strong id="b9442183111381"><a name="b9442183111381"></a><a name="b9442183111381"></a>M</strong> (minute), <strong id="b19442113133818"><a name="b19442113133818"></a><a name="b19442113133818"></a>H</strong> (hour), <strong id="b1044333113381"><a name="b1044333113381"></a><a name="b1044333113381"></a>D</strong> (day), and <strong id="b174431331173815"><a name="b174431331173815"></a><a name="b174431331173815"></a>W</strong> (week).</p>
</td>
</tr>
<tr id="row7779924473"><td class="cellrowborder" valign="top" width="17.18171817181718%" headers="mcps1.2.4.1.1 "><p id="p47808219478"><a name="p47808219478"></a><a name="p47808219478"></a>--forecast-method</p>
</td>
<td class="cellrowborder" valign="top" width="58.33583358335833%" headers="mcps1.2.4.1.2 "><p id="p1326221113472"><a name="p1326221113472"></a><a name="p1326221113472"></a>Forecast method</p>
</td>
<td class="cellrowborder" valign="top" width="24.48244824482448%" headers="mcps1.2.4.1.3 "><p id="p1478019234718"><a name="p1478019234718"></a><a name="p1478019234718"></a><strong id="b1039834216411"><a name="b1039834216411"></a><a name="b1039834216411"></a>auto_arima</strong> and <strong id="b8725644134118"><a name="b8725644134118"></a><a name="b8725644134118"></a>fbprophet</strong></p>
</td>
</tr>
<tr id="en-us_topic_0283137279_row1773402524719"><td class="cellrowborder" valign="top" width="17.18171817181718%" headers="mcps1.2.4.1.1 "><p id="en-us_topic_0283137279_p13734825204719"><a name="en-us_topic_0283137279_p13734825204719"></a><a name="en-us_topic_0283137279_p13734825204719"></a>--save-path</p>
</td>
<td class="cellrowborder" valign="top" width="58.33583358335833%" headers="mcps1.2.4.1.2 "><p id="en-us_topic_0283137279_p3734112544712"><a name="en-us_topic_0283137279_p3734112544712"></a><a name="en-us_topic_0283137279_p3734112544712"></a>Path for storing the forecast result</p>
</td>
<td class="cellrowborder" valign="top" width="24.48244824482448%" headers="mcps1.2.4.1.3 "><p id="p618154471812"><a name="p618154471812"></a><a name="p618154471812"></a>N/A</p>
</td>
</tr>
<tr id="en-us_topic_0283137279_row1068864085011"><td class="cellrowborder" valign="top" width="17.18171817181718%" headers="mcps1.2.4.1.1 "><p id="en-us_topic_0283137279_p1568814095019"><a name="en-us_topic_0283137279_p1568814095019"></a><a name="en-us_topic_0283137279_p1568814095019"></a>--version, -v</p>
</td>
<td class="cellrowborder" valign="top" width="58.33583358335833%" headers="mcps1.2.4.1.2 "><p id="en-us_topic_0283137279_p368834095019"><a name="en-us_topic_0283137279_p368834095019"></a><a name="en-us_topic_0283137279_p368834095019"></a>Current tool version</p>
</td>
<td class="cellrowborder" valign="top" width="24.48244824482448%" headers="mcps1.2.4.1.3 "><p id="p499654318184"><a name="p499654318184"></a><a name="p499654318184"></a>N/A</p>
</td>
</tr>
</tbody>
</table>

